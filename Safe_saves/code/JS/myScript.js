$(document).ready(function() {
    
    //Rest of the Javascript
    smoothScroll(1000);
    new WOW().init();
    
    
    $('#resume_anchor').waypoint(function(direction) {
        
        if (direction == "down") {
            $('#resume_spy').addClass('active');
            $('#biography_spy').removeClass('active');
            
        }
        else {
            $('#resume_spy').removeClass('active');
        }
        
    }, { offset: '50%' });
    
    $('#work_anchor').waypoint(function(direction) {
        
        if (direction == "down") {
            $('#work_spy').addClass('active');
            $('#resume_spy').removeClass('active');
        }
        else {
            $('#work_spy').removeClass('active');
            $('#resume_spy').addClass('active');
        }
        
    }, { offset: '50%' });

    

    
});


//Smooth Scroll Function from Document Ready Function
function smoothScroll(duration){
    $('a[href^="#"]').on('click', function(event) {
        
        var target = $( $(this).attr('href') );
        
        if( target.length) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, duration);            
        }
    }); 
    
    //alert('Please note: This website is under construction :D');
}

//Google analytics
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-56578667-1', 'auto');
          ga('send', 'pageview');

        
          