$(document).ready(function() {
    
    //Rest of the Javascript
    smoothScroll(1000);
    var browser_width = $(window).width();
    if(browser_width > 1000) {
        new WOW().init();
        
    }
    workBelt();
    workLoad();    
    
    var options = {
  useEasing : true, 
  useGrouping : true, 
  separator : ',', 
  decimal : '.'  
    }
    var numAnim1 = new countUp("glassOfBeer", 24.02, 99.99, 1 , 4.5, options);
    var numAnim2 = new countUp("linesOfCode", 24.02, 99.99, 1 , 4.5, options);
    var numAnim3 = new countUp("cupsOfCoffee", 24.02, 99.99, 1 , 4.5, options);
    var numAnim4 = new countUp("hoursOfSleep", 24.02, 99.99, 1 , 4.5, options);
    var numAnim5 = new countUp("sliceOfPizza", 24.02, 99.99, 1 , 4.5, options);
    
    
    
    $('#resume_anchor').waypoint(function(direction) {
        
        if (direction == "down") {
            $('#resume_spy').addClass('active');
            $('#biography_spy').removeClass('active');
            
        }
        else {
            $('#resume_spy').removeClass('active');
        }
        
    }, { offset: '50%' });
    
    $('#work_anchor').waypoint(function(direction) {
        
        if (direction == "down") {
            $('#work_spy').addClass('active');
            $('#resume_spy').removeClass('active');
        }
        else {
            $('#work_spy').removeClass('active');
            $('#resume_spy').addClass('active');
        }
        
    }, { offset: '50%' });
    
    $('#biography_anchor').waypoint(function(direction) {
        numAnim1.start();
        numAnim2.start();
        numAnim3.start();
        numAnim4.start();
        numAnim5.start();
        
        if (direction == "down") {
            $('#biography_spy').addClass('active');
            $('#work_spy').removeClass('active');
        }
        else {
            $('#biography_spy').removeClass('active');
            $('#work_spy').addClass('active');
        }
        
    }, { offset: '50%' });

    

     
});

function workLoad() {
    
    $.ajaxSetup({ cache: true });
    
    $('.thumb-units').click(function() {
        
        var $this = $(this),
            newTitle = $this.find('span').text(),
            newFolder = $this.data('folder'),
            spinner = '<div class="loader">Loading...</div>',
            newHTML = '../work_pages/' + newFolder + '.html';
        $('.project-load').html(spinner).load(newHTML);
        $('.project-title').text(newTitle)

    });
    
}



function workBelt() {
    $('.thumb-units').click(function() {
        $('.work-belt').css('left', '-150%');
        $('.project_container').show();
    });
    
    
    $('.work-return').click(function() {
        $('.work-belt').css('left', '0%');
        $('.project_container').hide(200);
        
    });
}

//Smooth Scroll Function from Document Ready Function
function smoothScroll(duration){
    $('a[href^="#"]').on('click', function(event) {
        
        var target = $( $(this).attr('href') );
        
        if( target.length) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, duration);            
        }
    }); 
    
    
    
    //alert('Please note: This website is under construction :D');
}

function offImage() {
        $('.onImages').show(500);
        $('.offImages').hide(500);
        $('.carousel').hide(1000);
    }
function onImage() {
        $('.onImages').hide(500);
        $('.offImages').show(500);
        $('.carousel').show(1000);
    }

//Google analytics ACTIVATE LATER
 /*   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-56578667-1', 'auto');
          ga('send', 'pageview');
*/
        
          