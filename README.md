# README #

## Introduction:  

My portfolio page - **[www.utkarshsaxena.com](www.utkarshsaxena.com)** (Deactivated - new link: utkarshsaxena.carbonmade.com)

## Technologies: ##

* HTML
* CSS/SASS
* JavaScript, JQuery
* AJAX

## Purpose: ##

Create a portfolio website to showcase my skills and project work.

## Procedure: ##

1. Browse to **[www.utkarshsaxena.com](www.utkarshsaxena.com)**  (deactivated - new link: utkarshsaxena.carbonmade.com)

## Timestamp: ##

**December 2014 – March 2015**